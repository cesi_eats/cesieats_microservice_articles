import { Card } from "../models/Card";
import { Category } from "../models/Category";
import {Restaurant} from "../models/Restaurant";
import logger from "jet-logger";
import {ObjectId} from "mongodb";

function getAllCategories(callback: Function) {
    Category.find().then((categories) => {
        callback(true, categories);
    }).catch((err) => {
        if (err) {
            logger.err(err);
        }
    })
}

async function addNewCategory(req: any, callback: Function) {

    const id = new ObjectId();
    const restaurantId = req.params.restaurantId;
    let newCategory = {
        _id: id,
        name: req.body.name
    }

    let category = new Category(newCategory)

    category.save().then(() => {
        logger.info(`Category ${req.body.name} created`);
    }).catch((err) => {
        if(err){
            logger.err(err);
        }
    })

    const restaurant = await Restaurant.findOne({_id:restaurantId}).exec();
    if(!restaurant){
        callback(false, null);
        return;
    }
    const cardId = restaurant.card;

    Card.findOne({_id:cardId}).then((card) => {
        if(!card){
            Promise.reject('Error no card for given id')
        }
    
        // @ts-ignore
        card.categories.push(newCategory._id)
    
        // @ts-ignore
        return Promise.all([card.save()]);
    
    }).then(()=>{
        callback(true, category);
    
    }).catch((err) => {
        if(err){
            logger.err(err);
        }
    })
}

function deleteCategoryById(categoryId: any, callback: Function) {
    if (!categoryId.match(/^[0-9a-fA-F]{24}$/)) {
        callback(false, "Wrong format of id");
        return;
    }
    Category.deleteOne({_id:categoryId}).then((result) => {
        if(result.deletedCount === 0){
            callback(false, null);
        }
        else{
            Category.find({ articles: { $in: [categoryId] } }).then(cards => {
                Promise.all(
                    cards.map(card =>
                        Card.findOneAndUpdate(
                        card._id,
                        { $pull: { categories: categoryId } },
                        { new: true }
                        )
                    )
                );
            });
            callback(true, "Category deleted");
        }
    }).catch((err) => {
        if (err) {
            logger.err(err);
        }
    })
}

function updateCategoryById(categoryId: any, body: any, callback: Function) {
    if (!categoryId.match(/^[0-9a-fA-F]{24}$/)) {
        callback(false, "Wrong format of id");
        return;
    }
    Category.findById(categoryId).then((category:any) => {
        if(!category) {
            callback(false, "Category not found");
            return;
        }
        else {
            if(body.name) {
                category.name = body.name;
            }
            category.save();
            callback(true, "Category updated");
        }
    })
}

export default {
    getAllCategories,
    addNewCategory,
    deleteCategoryById,
    updateCategoryById
} as const;