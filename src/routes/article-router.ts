import StatusCodes from 'http-status-codes';
import { Request, Response, Router } from 'express';
import {ObjectId} from "mongodb";
import articleService from '@services/article-service';
import { json } from 'body-parser';

const baseRoute = "/article";

// Constants
const router = Router();
const { CREATED, OK } = StatusCodes;

// Paths
export const p = {
    all: '/',
    add: '/:categoryId',
    get: '/:id',
    delete: '/:id',
    update : '/:id',
    getGroupByCategory: '/grouped/:restaurantId'
} as const;

//get all articles
router.get(p.all, (req, res) => {
    articleService.getAll((articles:any) =>{
        res.status(200).json({success: true, data: articles});
    });
})

//get article by category
router.get(p.getGroupByCategory, (req, res) => {
    if (!req.params.restaurantId.match(/^[0-9a-fA-F]{24}$/)) {
        res.status(400).send({success: false, error: 'Invalid id' });
        return;
    }
    articleService.getAllByCategory(req.params.restaurantId, (isSuccess:boolean, map:any) =>{
        if(isSuccess){
            res.status(200).json({success:true, data: Object.fromEntries(map)});
        }
        else{
             res.status(404).json({success:false, message: "No articles found"});
        }
    });      
})


//post an article
router.post(p.add, (req,res) => {
    articleService.addNewArticle(req,(isSuccess:boolean, article:any) =>{
        if(isSuccess){
            res.status(201).json({success: true, data: article});
        }
        else{
            res.status(400).json({success: false, message: "Error while adding article"});
        }
    })
})

//get an article by id
router.get(p.get, (req, res) => {
    articleService.getArticleById(req.params.id, (isSuccess:boolean, article:any) =>{
        if(isSuccess){
            res.status(200).json({success: true, data: article});
        }
        else{
            res.status(404).json({success: false, message: "No article found"});
        }
    })
})

//delete an article by id
router.delete(p.delete, (req, res) => {
    const articleId = req.params.id;
    articleService.deleteArticleById(articleId, (isSuccess:boolean, message:any) =>{
        if(isSuccess){
            res.status(200).json({success: true, message});
        }
        else{
            res.status(404).json({success: false, message});
        }
    })
})

//update an article by id
router.put(p.update, (req, res) => {
    const articleId = req.params.id;
    const body = req.body;
    articleService.updateArticleById(articleId, body, (isSuccess:boolean, message:any) =>{
        if(isSuccess){
            res.status(200).json({success: true, message});
        }
        else{
            res.status(400).json({success: false, message});
        }
    })
})

// Export default
export default {
    router,
    baseRoute
} as const;
