import mongoose, { Schema, Document } from "mongoose";
import { Card } from "./Card";

export interface IArticle extends Document {
    name: string;
    description: string;
    price: number;
    img: string;
}

const ArticleSchema: Schema = new Schema({

    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    img: {
        type: String,
        required: false
    }
})

const Article = mongoose.model<IArticle>("Articles", ArticleSchema);
export { Article };
