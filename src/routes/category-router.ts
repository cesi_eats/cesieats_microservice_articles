import StatusCodes from 'http-status-codes';
import { Request, Response, Router } from 'express';
import {ObjectId} from "mongodb";
import categoryService from "@services/category-service";

const baseRoute = "/category";


// Constants
const router = Router();
const { CREATED, OK } = StatusCodes;

// Paths
export const p = {
    all: '/',
    add: '/:restaurantId',
    get: "/:categoryId",
    update: "/:categoryId",
    delete: "/:categoryId"
} as const;


router.get(p.all, (req, res) => {
    categoryService.getAllCategories((isSuccess:boolean, categories:any) => {
        if(isSuccess){
            res.status(200).json({success: true, data: categories});
        }
    })
})

router.post(p.add, (req,res) => {
    categoryService.addNewCategory(req,(isSuccess:boolean, category:any) => {
        if(isSuccess){
            res.status(200).json({success: true, data: category});
        }
        else{
            res.status(400).json({success: false, message: "Error while adding category"});
        }
    })
})

router.delete(p.delete, (req,res) => {
    const categoryId = req.params.categoryId;
    categoryService.deleteCategoryById(categoryId, (isSuccess:boolean, message:any) => {
        if(isSuccess){
            res.status(200).json({success: true, message});
        }
        else{
            res.status(400).json({success: false, message});
        }
    })
})

router.put(p.update, (req, res) => {
    const categoryId = req.params.categoryId;
    const body = req.body;
    categoryService.updateCategoryById(categoryId, body, (isSuccess:boolean, message:any) =>{
        if(isSuccess){
            res.status(200).json({success: true, message});
        }
        else{
            res.status(400).json({success: false, message});
        }
    })
})


// Export default
export default {
    router,
    baseRoute
} as const;