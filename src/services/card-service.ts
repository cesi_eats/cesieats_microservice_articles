import { Card } from '../models/Card';
import { Restaurant } from '../models/Restaurant';
import logger from 'jet-logger';

function getAllCards(callback: Function) {
    Card.find().then((cards) => {
        callback(true, cards);
    }).catch(err => {
        if (err) {
            logger.err(err);
        }
    })
}

function addNewCard(req: any, id: any, callback: Function) {
    if (!id.match(/^[0-9a-fA-F]{24}$/)) {
        callback(false, "Wrong format of id");
        return;
    }
    const restaurantId = req.params.restaurantId;
    let newCard = {
        _id: id
    }

    let card = new Card(newCard)

    card.save().then(() => {
        logger.info(`Card created`);
    }).catch((err) => {
        if(err){
            logger.err(err);
        }
    })

    Restaurant.findOne({_id:restaurantId}).then((restaurant) => {

        if(!restaurant){
            Promise.reject('Error no restaurant for given id')
        }
    
        // @ts-ignore
        restaurant.cards.push(newCard._id)
    
        // @ts-ignore
        return Promise.all([restaurant.save()]);
    
    }).then((data)=>{
    
        callback(true, data);
    
    }).catch((err) => {
        if(err){
            logger.err(err);
        }
    })
}

function deleteCardById(cardId: any, callback: Function) {
    if (!cardId.match(/^[0-9a-fA-F]{24}$/)) {
        callback(false, "Wrong format of id");
        return;
    }
    Card.deleteOne({_id:cardId}).then((result) => {
        if(result.deletedCount === 0){
            callback(false, "Error while deleting card");
        }
        else{
            Card.find({ articles: { $in: [cardId] } }).then(restaurants => {
                Promise.all(
                    restaurants.map(restaurant =>
                        Restaurant.findOneAndUpdate(
                        restaurant._id,
                        { $pull: { cards: cardId } },
                        { new: true }
                        )
                    )
                );
            });
            callback(true, "Card deleted");
        }
    }).catch((err) => {
        if (err) {
            logger.err(err);
        }
    })
}

function updateCardById(cardId: any, body: any, callback: Function) {
    if (!cardId.match(/^[0-9a-fA-F]{24}$/)) {
        callback(false, "Wrong format of id");
        return;
    }
    Card.findById(cardId).then((card:any) => {
        if(!card) {
            callback(false, "Card not found");
            return;
        }
        else {
            card.save();
            callback(true, "Card updated");
        }
    })
}

export default {
    getAllCards,
    addNewCard,
    deleteCardById,
    updateCardById
} as const;