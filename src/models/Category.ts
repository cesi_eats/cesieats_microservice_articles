import mongoose, { Schema, Document } from "mongoose";

export interface ICategory extends Document {
    name: string;
    articles: Array<mongoose.Types.ObjectId>;
}

const CategorySchema: Schema = new Schema({
    name: {
        type: String,
        required: true
    },
    articles: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Article'
    }]
})

const Category = mongoose.model<ICategory>("Category", CategorySchema);
export { Category };