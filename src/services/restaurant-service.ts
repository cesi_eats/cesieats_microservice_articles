import { Restaurant } from '../models/Restaurant';
import logger from 'jet-logger';
import {Card} from "../models/Card";

function getAllRestaurants(callback: Function) {
    Restaurant.find().then((restaurants) => {
        callback(true, restaurants);
    }).catch(err => {
        if (err) {
            logger.err(err);
        }
    })
}

async function addNewRestaurant(req: any, callback: Function) {

    let card = new Card({});
    await card.save();

    let newRestaurant = {
        name: req.body.name,
        description : req.body.description,
        ownerId : req.body.ownerId,
        category: req.body.category,
        img: req.body.img,
        address: {
            number: req.body.address.number,
            street: req.body.address.street,
            zipCode: req.body.address.zipCode,
            city: req.body.address.city
        },
        card: card._id
    }

    let restaurant = new Restaurant(newRestaurant)

    restaurant.save().then(() => {
        logger.info(`Restaurant ${req.body.name} created`);
    }).catch((err) => {
        if(err){
            logger.err(err);
        }
    })
    callback(true, "New restaurant successfully created");
}

function getRestaurantById(restaurantId: any, callback: Function) {
    if (!restaurantId.match(/^[0-9a-fA-F]{24}$/)) {
        callback(false, "Wrong format of id");
        return;
    }
    Restaurant.findOne({_id:restaurantId}).then((restaurant) => {
        if(!restaurant){
            callback(false, null);
        }
        else{
            callback(true, restaurant);
        }
    }).catch((err) => {
        if (err) {
            logger.err(err);
        }
    })
}

function deleteRestaurantById(restaurantId: any, callback: Function) {
    if (!restaurantId.match(/^[0-9a-fA-F]{24}$/)) {
        callback(false, "Wrong format of id");
        return;
    }
    Restaurant.deleteOne({_id:restaurantId}).then((result) => {
        if(result.deletedCount === 0){
            callback(false, "Error while deleting restaurant");
        }
        else{
            callback(true, "Restaurant deleted");
        }
    }).catch((err) => {
        if(err){
            logger.err(err);
        }
    })
}

function updateRestaurantById(restaurantId: any, body: any, callback: Function) {
    if (!restaurantId.match(/^[0-9a-fA-F]{24}$/)) {
        callback(false, "Wrong format of id");
        return;
    }
    Restaurant.findById(restaurantId).then((restaurant:any) => {
        if(!restaurant) {
            callback(false, "Restaurant not found");
            return;
        }
        else {
            if(body.name) {
                restaurant.name = body.name;
            }
            if(body.description) {
                restaurant.description = body.description;
            }
            if(body.category) {
                restaurant.category = body.category;
            }
            if(body.img) {
                restaurant.img = body.img;
            }
            if(body.address) {
                if(body.address.number) {
                    restaurant.address.number = body.address.number;
                }
                if(body.address.street) {
                    restaurant.address.street = body.address.street;
                }
                if(body.address.zipCode) {
                    restaurant.address.zipCode = body.address.zipCode;
                }
                if(body.address.city) {
                    restaurant.address.city = body.address.city;
                }
            }
            restaurant.save();
            callback(true, "Restaurant updated");
        }
    })
}

export default {
    getAllRestaurants,
    addNewRestaurant,
    getRestaurantById,
    deleteRestaurantById,
    updateRestaurantById
} as const;