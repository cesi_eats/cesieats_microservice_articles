import { Router } from 'express';
import articleRouter from "@routes/article-router";
import cardRouter from "@routes/card-router";
import categoryRouter from "@routes/category-router";
import restaurantRouter from "@routes/restaurant-router";


const apiRouter = Router();


apiRouter.use(articleRouter.baseRoute, articleRouter.router);

apiRouter.use(cardRouter.baseRoute, cardRouter.router);

apiRouter.use(categoryRouter.baseRoute, categoryRouter.router);

apiRouter.use(restaurantRouter.baseRoute, restaurantRouter.router);

export default {
    apiRouter,
} as const;