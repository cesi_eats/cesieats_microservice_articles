import {Article} from "../models/Article";
import {Category} from "../models/Category";
import {Card} from "../models/Card";
import {Restaurant} from "../models/Restaurant";
import logger from "jet-logger";
import {ObjectId} from "mongodb";

function getAll(callback:Function) {
    Article.find().then((articles) => {
        callback(articles);

    }).catch((err) => {
        if (err) {
            logger.err(err);
        }
    })
}

async function getAllByCategory(restaurantId:any, callback:Function) {
    if (!restaurantId.match(/^[0-9a-fA-F]{24}$/)) {
        callback(false, "Wrong format of id");
        return;
    }

    const map = new Map();
    const restaurant = await Restaurant.findOne({_id:restaurantId}).exec();
    if(!restaurant){
        callback(false, null);
        return;
    }
    const cardId = restaurant.card;

    Card.findOne({_id:cardId}).then((card) => {
        if(!card){
            callback(false, null);
        }
        else{
            // @ts-ignore
            Category.find().where('_id').in(card.categories).exec().then(async (categories) => {
                for(let i = 0; i < categories.length; i++){
                    const articles = await Article.find().where('_id').in(categories[i].articles).exec()
                    let data = {
                        "categoryId" : categories[i]._id,
                        "articles" : articles
                    }
                    map.set(categories[i].name, data);
                }
                callback(true, map);
            })
        }
    }
)}

function addNewArticle(request:any, callback:Function) {

    const id = new ObjectId();
    const body = request.body;
    const categoryId = request.params.categoryId;
    // @ts-ignore
    const newArticle = {
        _id: id,
        name: body.name,
        description: body.description,
        price: body.price,
        img: body.img
    }
    Category.findOne({_id:categoryId}).then((category) => {

        if(!category){

            callback(false, null);
        }
        else{
            // @ts-ignore
            category.articles.push(newArticle._id)
            // @ts-ignore
            return Promise.all([new Article(newArticle).save(), category.save()]);
        }
    }).then((data)=>{

        callback(true, data);

    }).catch((err) => {
        if(err){
            logger.err(err);
        }
    })
}

function getArticleById(articleId: any, callback: Function) {
    if (!articleId.match(/^[0-9a-fA-F]{24}$/)) {
        callback(false, "Wrong format of id");
        return;
    }
    Article.findOne({_id:articleId}).then((article) => {
        if(!article){
            callback(false, null);
        }
        else{
            callback(true, article);
        }
    }).catch((err) => {
        if (err) {
            logger.err(err);
        }
    })
}

function deleteArticleById(articleId: any, callback: Function) {
    if (!articleId.match(/^[0-9a-fA-F]{24}$/)) {
        callback(false, "Wrong format of id");
        return;
    }
    Article.deleteOne({_id:articleId}).then((result) => {
        if(result.deletedCount === 0){
            callback(false, "No article found");
        }
        else{
            Category.find({ articles: { $in: [articleId] } }).then(categories => {
                Promise.all(
                    categories.map(category =>
                        Category.findOneAndUpdate(
                        category._id,
                        { $pull: { articles: articleId } },
                        { new: true }
                        )
                    )
                );
            });
            callback(true, "Article deleted");
        }
    }).catch((err) => {
        if (err) {
            logger.err(err);
        }
    })
}

function updateArticleById(articleId: any, body: any, callback: Function) {
    if (!articleId.match(/^[0-9a-fA-F]{24}$/)) {
        callback(false, "Wrong format of id");
        return;
    }
    Article.findById(articleId).then((article:any) => {
        if(!article) {
            callback(false, "Article not found");
            return;
        }
        else {
            if(body.name) {
                article.name = body.name;
            }
            if(body.description) {
                article.description = body.description;
            }
            if(body.price) {
                article.price = body.price;
            }
            if(body.img) {
                article.img = body.img;
            }
            article.save();
            callback(true, "Article updated");
        }
    })
}

export default {
    getAll,
    getAllByCategory,
    addNewArticle,
    getArticleById,
    deleteArticleById,
    updateArticleById
} as const;