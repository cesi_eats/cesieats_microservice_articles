import mongoose, { Schema, Document } from "mongoose";

export interface IRestaurant extends Document {
    name: string;
    description: string;
    ownerId: number;
    category: string;
    address: {
        number: number;
        street: string;
        zipCode: number;
        city: string;
    }
    card: mongoose.Types.ObjectId;
}

const RestaurantSchema: Schema = new Schema({
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    ownerId: {
        type: Number,
        required: true
    },
    category: {
        type: String,
        required: true
    },
    img: {
        type: String,
        required: false
    },
    address: {
        number: {
            type: Number,
            required: true
        },
        street: {
            type: String,
            required: true
        },
        zipCode: {
            type: Number,
            required: true
        },
        city: {
            type: String,
            required: true
        }
    },
    card: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Card'
    }
})

const Restaurant = mongoose.model<IRestaurant>("Restaurant", RestaurantSchema);
export { Restaurant };