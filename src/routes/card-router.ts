import StatusCodes from 'http-status-codes';
import { Request, Response, Router } from 'express';
import {ObjectId} from "mongodb";
import cardService from "@services/card-service";

const baseRoute = "/card";


// Constants
const router = Router();
const { CREATED, OK } = StatusCodes;

// Paths
export const p = {
    all: '/',
    add: '/:restaurantId',
    get: "/:cardId",
    update: "/:cardId",
    delete: "/:cardId"
} as const;

router.get(p.all, (req, res) => {
    cardService.getAllCards((isSuccess:boolean, cards:any) => {
        if(isSuccess){
            res.status(200).json({success: true, data: cards})
        }
    })
})

router.post(p.add, (req,res) => {

    const id = new ObjectId();

    cardService.addNewCard(req, id, (isSuccess:boolean, card:any) => {
        if(isSuccess){
            res.status(200).json({success: true, data: card});
        }
        else{
            res.status(400).json({success: true, message: "Error while adding card"});
        }
    })
})

router.delete(p.delete, (req,res) => {
    const cardId = req.params.cardId;
    cardService.deleteCardById(cardId, (isSuccess:boolean, message:any) => {
        if(isSuccess){
            res.status(200).json({success: true, message});
        }
        else{
            res.status(400).json({success: false, message});
        }
    })
})

router.put(p.update, (req, res) => {
    const cardId = req.params.cardId;
    const body = req.body;
    cardService.updateCardById(cardId, body, (isSuccess:boolean, message:any) =>{
        if(isSuccess){
            res.status(200).json({success: true, message});
        }
        else{
            res.status(400).json({success: false, message});
        }
    })
})


// Export default
export default {
    router,
    baseRoute
} as const;
