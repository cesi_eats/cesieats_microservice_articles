import StatusCodes from 'http-status-codes';
import { Request, Response, Router } from 'express';
import restaurantService from '@services/restaurant-service';

const baseRoute = "/restaurant";

// Constants
const router = Router();
const { CREATED, OK } = StatusCodes;

// Paths
export const p = {
    all: '/',
    add: '/',
    get: "/:restaurantId",
    update: "/:restaurantId",
    delete: "/:restaurantId"
} as const;

router.get(p.all, (req, res) => {
    restaurantService.getAllRestaurants((isSuccess:boolean, restaurants:any) => {
        if(isSuccess){
            res.status(200).json({success: true, data: restaurants});
        }
    } )
})

router.post(p.add, (req,res) => {
    restaurantService.addNewRestaurant(req, (isSuccess:boolean, restaurant:any) => {
        if(isSuccess){
            res.status(200).json({success: true, data: restaurant});
        }
        else{
            res.status(400).json({success: false, message: "Error while adding restaurant"});
        }
    } )

})

router.get(p.get, (req, res) => {
    restaurantService.getRestaurantById(req.params.restaurantId, (isSuccess:boolean, restaurant:any) =>{
        if(isSuccess){
            res.status(200).json({success: true, data: restaurant});
        }
        else{
            res.status(404).json({success: false, message: "No restaurant found"});
        }
    })
})

router.delete(p.delete, (req,res) => {
    const restaurantId = req.params.restaurantId;
    restaurantService.deleteRestaurantById(restaurantId, (isSuccess:boolean, message:any) => {
        if(isSuccess){
            res.status(200).json({success: true, message});
        }
        else{
            res.status(400).json({success: false, message });
        }
    })
})

router.put(p.update, (req, res) => {
    const restaurantId = req.params.restaurantId;
    const body = req.body;
    restaurantService.updateRestaurantById(restaurantId, body, (isSuccess:boolean, message:any) =>{
        if(isSuccess){
            res.status(200).json({success: true, message});
        }
        else{
            res.status(400).json({success: false, message});
        }
    })
})


// Export default
export default {
    router,
    baseRoute
} as const;