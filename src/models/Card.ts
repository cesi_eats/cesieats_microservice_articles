import mongoose, { Schema, Document } from "mongoose";

export interface ICard extends Document {
    categories: Array<mongoose.Types.ObjectId>;
}

const CardSchema: Schema = new Schema({
        categories: [{
            type: mongoose.Schema.Types.ObjectId,
            ref : 'Category'
        }]
})

const Card = mongoose.model<ICard>("Cards", CardSchema);
export { Card };